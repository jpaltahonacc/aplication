import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { Icon  } from 'native-base';

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  return (
    <Icon
      name={props.name}
      size={26}
    />
  );
}
