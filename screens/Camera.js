import React from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity,  Dimensions } from 'react-native';

import { Container, Header } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import { Icon  } from 'native-base';
import Form from '../components/Form';



const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default class Camara extends React.Component {
  camera = null;
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    switchValue: false,
    photos: {}
  }

  async componentDidMount(){
    const camera = await Permissions.askAsync(Permissions.CAMERA);
    const audio = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    const hasCameraPermission = ( camera.status === 'granted' && audio.status === 'granted');

    this.setState({ hasCameraPermission });
  }
  capturePhoto = async () => {
    if (this.camera) {
      let photo = await this.camera.takePictureAsync();
      this.setState({
        photos: photo
      });
    }}
  fip = () => {
    this.setState({
        type:
        this.state.type === Camera.Constants.Type.back
            ? Camera.Constants.Type.front
            : Camera.Constants.Type.back,
    });
  }

  render(){
    
    const { hasCameraPermission } = this.state;

    if (hasCameraPermission === null) {
        return <View />;
    } else if (hasCameraPermission === false) {
        return <Text>Access to camera has been denied.</Text>;
    }
      return (
          <Container>
              <Grid>
                <Col style={{ backgroundColor: '#dcdcdc', height: '100%', marginTop: 24 }}>
                    <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref; }} />
                        
                    <View style={styles.optionCamera}>
                       {/*
                        <TouchableOpacity style={ styles.rotar} onPress={this.fip}>
                            <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip </Text>
                        </TouchableOpacity>
                    */} 
                        <TouchableOpacity style={styles.tomar}
                            onPress={this.capturePhoto }>
                            <View style={styles.photo}/>
                        </TouchableOpacity>
                    </View>
                </Col>
              </Grid>
          </Container>
            
        );}
}

Camara.navigationOptions = {
  title: null,
  header: null,
};

const styles = StyleSheet.create({

  preview: {
    flex: 1,
    height:500,
    width: winWidth,
    position: 'relative',
    top: 24,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    
  },
  rotar:{},
  tomar:{
      backgroundColor: '#ffffff9c',
      borderRadius: 50,
      position: 'relative',
      bottom: 10,
      width: 80,
      height: 80,
      textAlign: 'center',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
  },
  photo: {
    width: '70%',
    height: '70%',
    backgroundColor: '#ffffff',
    borderRadius: 100
  },
  optionCamera: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        padding: 20
  },
  contents: {
    position: 'relative',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    overflow: 'hidden',
    backgroundColor: '#e6e6e6',
    flex: 1
  }
});
