import React ,  { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  StatusBar
} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right  } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';


export default class HomeScreen extends Component{

    render() {
       
        return (
            <Container>
               
                <Header />
                <Content>
                    <Grid>
                        <Col>
                            <Card>
                                <CardItem>
                                    <Text>hello</Text>
                                </CardItem>
                                <CardItem cardBody>
                                    <Image source={{uri: 'https://maderoatelier.com/wp-content/uploads/2019/02/madero-atelier-muebles-ba%C3%B1o-velvet-2-686x1030.jpg'}} style={{height: 200, width: null, flex: 1}}/>
                                </CardItem>
                                <CardItem>
                                    <Left>
                                        <Button transparent>
                                        <Icon  name="thumbs-up" />
                                        <Text>12 Likes</Text>
                                        </Button>
                                    </Left>
                                    <Body>
                                        <Button transparent>
                                        <Icon name="chatbubbles" />
                                        <Text>4 </Text>
                                        </Button>
                                    </Body>
                                    
                                </CardItem>
                            </Card>
                        </Col>
                    </Grid>
                    
                </Content>
                
            </Container>
          );
    }
}

HomeScreen.navigationOptions = {
  header: null,
};
